/** Created by Koray Sels
 * test voorbeeld:
 ```
 $ npm start
 ```
 **/
import fs from 'node:fs/promises';
import path from 'node:path';

function main(dir: string) {
    console.log(`directory "${dir}" contains following files:`);
    return fs
        .readdir(dir)
        .then((files: string[]) =>
            files
                .map((file: string) => path.join(dir, file))
        );
}

console.log("Hello from typescript to nodeJS");
console.log(await main("."))
